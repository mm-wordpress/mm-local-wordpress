#!/bin/bash
################################################################################
# File : install_local_wordpress.sh
#
# Usage: ./install_local_wordpress.sh
#
# Description: Create and run a local installation of a WordPress project.
#
#              - Create a MariaDB/MySQL database. 
#              - Create project structure.
#                 -  content/ → wp-content equivalent
#                 - index.php → WordPress view bootstrapper
#                 -       wp/ → WordPress core (never edit)
#              - Download the latest version of WordPress.
#              - Clean themes and plugins by default.
#              - Runs the standard WordPress installation process.
#              - Create and configure Virtual Host.
#              - Generate a self-signed Certificate.
#              - Add to /etc/hosts file hostnames.
#
#              Compatible and tested with Fedora Workstation 31.
#
# Author:  Moisés Muñoz <mnmoises@gmail.com>
# Created: 27-04-2020
################################################################################

# ------------------------------------------------------------------------------
# | VARIABLES                                                                  |
# ------------------------------------------------------------------------------
WORKING_DIR="/home/youruser/www/"  # Working directory - Feel free to change it.
ETC_VHOSTS="/etc/httpd/conf.d/"
ETC_HOSTS="/etc/hosts"
IP="127.0.0.1"

# ------------------------------------------------------------------------------
# | MAIN FUNCTIONS                                                             |
# ------------------------------------------------------------------------------

# Prompts you for the name of the project.
enterProjectName() {  
  read -r -e -p 'Project Name: ' projname

  # Check that is not empty, starts with a dash and does not contain special characters
  while [[ -z "$projname" || "$projname" == *['!'@#\$%^\&*()_+]* || "$projname" == "-" ]]; do
    echo -e "\e[1;33mWarning:\e[0m The project name can't be empty or contain special character.\n"
    exit 1
  done

  # We keep the answers in this variable, in lowercase and without blank spaces.
  PROJECT_NAME=$(echo "${projname,,}" | sed 's/ //g')
}


# Prompts you for the website name.
enterSiteName() {
  read -r -e -p 'Site Name: ' sitename

  # Check that is not empty, starts with a dash and does not contain special characters
  while [[ -z "$sitename" || "$sitename" == *['!'@#\$%^\&*()_+]* || "$sitename" == "-" ]]; do
    echo -e "\e[1;33mWarning:\e[0m The site name can't be empty or contain special character.\n"
    exit 1
  done
}


# Prompts you for the email for your ServerAdmin and WordPress admin user.
enterYourEmail() {
  read -r -e -p 'Your Email: ' youremail

  # Check that the email is valid.
  if [[ "$youremail" =~ ^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$ ]]; then
    echo -e "\n\e[1mInfo:\e[0m Email address '$youremail' is valid."
  else
    echo -e "\e[1;33mWarning:\e[0m Email address '$youremail' is invalid."
    exit 1
  fi
}


# Create a MariaDB/MySQL database.
installDatabase() {
  while true; do
    read -r -e -p "Do you need to setup new MySQL database? [y/N]: " yn
    case $yn in
      [yY][eE][sS]|[yY])    
        dbsetup="CREATE DATABASE $dbname; CREATE USER '$dbuser'@'$dbhost' IDENTIFIED BY '$dbpass'; GRANT ALL PRIVILEGES ON $dbname.* TO '$dbuser'@'$dbhost'; FLUSH PRIVILEGES;"
        echo -e "\n\e[1mInfo:\e[0m Creating database and user..."
	      mysql -u root -p -e "$dbsetup"
        echo -e "\e[1;32mSuccess:\e[0m The database '$dbname' and the user '$dbuser' have been created."
        break 2
      ;;
      [nN][oO]|[nN])
        echo -e "\e[1;33mWarning:\e[0m Database creation has been cancelled."
        break 2
      ;;
      *) echo -e "\e[1;33mPlease answer yes or no\e[0m"
      ;;
    esac
  done
}


# Create the project work folder.
createProject() {
  if [ ! -d "${projectdir}" ]; then
    mkdir "${projectdir}"
    echo -e "\e[1;32mSuccess:\e[0m The '${PROJECT_NAME}.local' project folder has been created."
  else
    echo -e "\e[1mInfo:\e[0m The '${PROJECT_NAME}.local' project folder already exists."
  fi
}

# Downloads core WordPress files using WP-CLI.
downloadWordPress() {
  cd "${projectdir}"
  echo -e "\e[1mInfo:\e[0m Project directory: '$PWD'."
  if [ ! -d "${coredir}" ]; then
    wp core download --path=wp
  else
    echo -e "\e[1;32mSuccess:\e[0m WordPress is already installed on '$coredir'"
  fi  
}

# WordPress view bootstrapper.
# Fix to recognize the new WordPress core installation folder.
mainIndex() {
  if [ ! -f "${projectdir}"/index.php ]; then
    cp "${coredir}"/index.php "${projectdir}"/.
    cd "${projectdir}"
    sed -i "s%/wp-blog-header.php%/wp/wp-blog-header.php%g" "index.php"
    echo -e "\e[1;32mSuccess:\e[0m Created the main 'index.php' file."
  else
    echo -e "\e[1;32mSuccess:\e[0m The main 'index.php' already exist."
  fi
}

# Generates a wp-config.php file.
wpconfig() {
  if [ -f "${projectdir}"/wp-config.php ]; then
    echo -e "\e[1;32mSuccess:\e[0m The 'wp-config.php' file already exists."
  else
    echo -e "\e[1mInfo:\e[0m Generating wp-config.php file ..."	  
    wp config create --dbname=$dbname --dbuser=$dbuser --dbpass="$dbpass" --dbhost=localhost --extra-php <<PHP
/**
 * Fix to set \$_SERVER['HTTP_HOST'] in the WP-CLI context
 */
if ( defined( 'WP_CLI' ) ) {
    \$_SERVER['HTTP_HOST'] = 'localhost';
}

/**
 * Upload files, install plugins or themes without entering the FTP data.
 * IMPORTANT: Only for local development. Comment this line for production environments.
 */
define( 'FS_METHOD', 'direct' );

/**
 * WordPress URLs.
 */
define( 'WP_SITEURL', 'https://' . \$_SERVER['HTTP_HOST'] . '/wp' );
define( 'WP_HOME', 'https://' . \$_SERVER['HTTP_HOST'] );

/**
 * Custom Directory Locations.
 */
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/content' );
define( 'WP_PLUGIN_DIR', dirname( __FILE__ ) . '/content/plugins' );
define( 'WP_CONTENT_URL', 'https://' . \$_SERVER['HTTP_HOST'] . '/content' );
define( 'WP_PLUGIN_URL', 'https://' . \$_SERVER['HTTP_HOST'] . '/content/plugins' );

PHP
    mv "${coredir}"/wp-config.php "${projectdir}"/.
  fi
}

# Create the content folder: wp-content equivalent.
contentDirectory() {
  if [ ! -d "$contentdir" ]; then
    cp -R "${coredir}"/wp-content "${projectdir}"/.
    mv "${projectdir}"/wp-content "${projectdir}"/content
    echo -e "\e[1;32mSuccess:\e[0m Created 'content/' folder on '${contentdir}'"
  fi
}

cleanContentDir() {	
  # Delete Themes and Plugins by default inside of 'wp/' folder
  rm -Rf "${coredir}"/wp-content/themes/twenty*
  rm -Rf "${coredir}"/wp-content/plugins/akismet
  rm -f "${coredir}"/wp-content/plugins/hello.php
  echo -e "\e[1;32mSuccess:\e[0m Deleted themes and plugins by default on '${coredir}'"
  # Delete Themes and Plugins inside of 'content/' folder
  rm -R "${contentdir}"/themes/twentys*
  rm -R "${contentdir}"/themes/twentynineteen
  rm -Rf "${contentdir}"/plugins/akismet
  rm -f "${contentdir}"/plugins/hello.php
  echo -e "\e[1;32mSuccess:\e[0m Deleted themes and plugins by default on '${contentdir}'\n"
}

# Runs the standard WordPress installation process using WP-CLI.
installWordPress() {
  while true; do
    read -r -e -p "Do you want to use the default settings? [y/N] " setupdefault
    case ${setupdefault} in
      [yY][eE][sS]|[yY])
        wp core install --title="$sitename" --url=localhost/${PROJECT_NAME} --admin_user=$adminuser --admin_password="$adminpass" --admin_email=$adminemail --skip-email
        echo -e "\e[1;32mSuccess:\e[0m The default configuration has been applied."
        echo -e "\e[0;34m=================================================\e[0m"
        echo -e "\e[0;34m#     Access credentials WordPress dashboard    #\e[0m"
        echo -e "\e[0;34m=================================================\e[0m"
        echo -e "\e[1;33mUsername:\e[0m '$adminuser'"
        echo -e "\e[1;33mPassword:\e[0m '$adminpass'"
        echo -e "\e[1;33mEmail:\e[0m '$adminemail'"
        echo -e "\e[0;34m=================================================\e[0m"
        break 2
      ;;
      [nN][oO]|[nN])
        echo -e "\e[1;33mWarning:\e[0m You will have to configure the access credentials WordPress panel the first time you load the URL in your browser: localhost/${PROJECT_NAME}.local "
        break 2
      ;;
      *) echo -e "\e[1;33mPlease answer yes or no\e[0m"
      ;;
    esac
  done
}

# Create a new Virual Host for your local WordPress project.
addVirtualHost() {
  if [ ! -f "${ETC_VHOSTS}${PROJECT_NAME}.local.conf" ]; then
    echo -e "\e[1mInfo:\e[0m Creating virtual host in '${ETC_VHOSTS}\e[1m${PROJECT_NAME}.local.conf'\e[0m"

    sudo bash -c "cat <<EOF > ${ETC_VHOSTS}${PROJECT_NAME}.local.conf
    <VirtualHost *:80>
      ServerName ${PROJECT_NAME}.local
      ServerAdmin ${youremail}
      DocumentRoot ${WORKING_DIR}${PROJECT_NAME}.local

      <Directory ${WORKING_DIR}${PROJECT_NAME}.local>
          Options Indexes FollowSymLinks
          AllowOverride None
          Require all granted
      </Directory>

      ErrorLog /var/log/httpd/vhost/${PROJECT_NAME}.local_error_log
      CustomLog /var/log/httpd/vhost/${PROJECT_NAME}.local_access_log combined
    </VirtualHost>

    <VirtualHost _default_:443>
      ServerName ${PROJECT_NAME}.local
      ServerAdmin ${youremail}
      DocumentRoot ${WORKING_DIR}${PROJECT_NAME}.local

      # SSL Configuration
      SSLEngine on
      SSLCertificateFile /etc/pki/tls/certs/${PROJECT_NAME}.local.crt
      SSLCertificateKeyFile /etc/pki/tls/private/${PROJECT_NAME}.local.key
    
      <Directory ${WORKING_DIR}${PROJECT_NAME}.local>
          Options Indexes FollowSymLinks
          AllowOverride None
          Require all granted

          # BEGIN WordPress
          RewriteEngine On
          RewriteBase /
          RewriteRule ^index\.php$ - [L]
          RewriteCond %{REQUEST_FILENAME} !-f
          RewriteCond %{REQUEST_FILENAME} !-d
          RewriteRule . /index.php [L]
          # END WordPress
      </Directory>

      ErrorLog /var/log/httpd/vhost/${PROJECT_NAME}.local_error_log
      CustomLog /var/log/httpd/vhost/${PROJECT_NAME}.local_access_log combined
    </VirtualHost>
EOF"
  else
    echo -e "\e[1;33mWarning:\e[0m The virtual host '${ETC_VHOSTS}${PROJECT_NAME}.local.conf' already exists."
  fi
  # Change permissions for Apache.
  sudo chown apache:apache /etc/httpd/conf.d/"${PROJECT_NAME}".local.conf
  echo -e "\e[1;32mSuccess:\e[0m Changed permissions to apache:apache for ${PROJECT_NAME}.local.conf"
}

# Generate a Self-Signed Certificate for the project.
generateSelfSignedCertificate() {
  sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
                   -out /etc/pki/tls/certs/"${PROJECT_NAME}".local.crt \
                   -keyout /etc/pki/tls/private/"${PROJECT_NAME}".local.key \
                   -subj "/C=ES/ST=./L=./O=./OU=./CN=."
}

# Add to /etc/hosts file hostnames.
addHost() {
  HOSTS_LINE="$IP   ${PROJECT_NAME}.local"

  if [ -n "$(grep "${PROJECT_NAME}".local ${ETC_HOSTS})" ]; then
    echo -e "\e[1;33mWarning:\e[0m ${PROJECT_NAME}.local already exists : $(grep "${PROJECT_NAME}".local $ETC_HOSTS)"
  else
    echo -e "\e[1mInfo:\e[0m Adding ${PROJECT_NAME}.local to your $ETC_HOSTS";
    sudo -- sh -c -e "echo '$HOSTS_LINE' >> ${ETC_HOSTS}";

    if [ -n "$(grep "${PROJECT_NAME}".local ${ETC_HOSTS})" ]; then
      echo -e "\e[1mInfo:\e[0m $(grep "${PROJECT_NAME}".local ${ETC_HOSTS})"
      echo -e "\e[1;32mSuccess:\e[0m ""${PROJECT_NAME}"".local was added succesfully."
    else
      echo -e "\e[1;33mWarning:\e[0m Failed to add ${PROJECT_NAME}.local, Try again!"
    fi
  fi
}


# ------------------------------------------------------------------------------
# | INITIALIZE PROGRAM                                                         |
# ------------------------------------------------------------------------------

main() {
  # Clear the screen.
  clear

  echo -e "\e[0;34m=========================================================\e[0m"
  echo -e "\e[0;34m#             WordPress Local Installation              #\e[0m"
  echo -e "\e[0;34m=========================================================\e[0m"

  # Enter your project data
  enterProjectName
  enterSiteName
  enterYourEmail
  
  # Saved Variables
  projectdir=${WORKING_DIR}/${PROJECT_NAME}.local
  coredir=${projectdir}/wp
  contentdir=${projectdir}/content

  # Default Database Parameters
  dbname="${PROJECT_NAME,,}"
  dbuser="${PROJECT_NAME,,}user"
  dbpass="${PROJECT_NAME,,}pass"
  dbhost="localhost"

  echo -e "\e[1;34m=========================================================\e[0m"
  echo -e "\e[1;34m#              Let's install the Database               #\e[0m"
  echo -e "\e[1;34m=========================================================\e[0m"

  # Start Apache
  echo -e "\e[1mInfo:\e[0m Starting Apache..."
  sudo systemctl start httpd.service

  installDatabase

  # Generate your project
  createProject              # Create project folder if not exist
  downloadWordPress          # Download the latest WordPress version
  mainIndex                  # Create/Check if exist main 'index.php' file
  wpconfig                   # Create/Check wp-config.php file if not exist
  contentDirectory           # Create/Check if exist 'content/' folder
  cleanContentDir            # Delete themes and plugins for a clean installation

  # Virtual host, Self-Signed Certificate and hostname.
  addVirtualHost
  echo -e "\e[1;32mSuccess:\e[0m Created Virtual Host $PROJECT_NAME.local.conf.\n"  
  generateSelfSignedCertificate
  echo -e "\e[1;32mSuccess:\e[0m Generated a self-signed certificate for the $PROJECT_NAME.local project.\n"
  addHost
  
  # Default Site Parameters
  adminuser="test"
  adminpass="test"
  adminemail="${youremail}"

  # Runs the standard WordPress installation process.
  installWordPress

  sudo chown -R moises:apache "${projectdir}"

  # Reset Apache and php-fpm.
  sudo systemctl restart php-fpm.service
  sudo systemctl restart httpd.service
}

# Run main program
main

exit